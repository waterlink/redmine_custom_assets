require 'redmine'
require_dependency 'custom_assets/hooks'

Redmine::Plugin.register :custom_assets do
  name 'Custom assets for redmine'
  author 'waterlink at smart cloud solutions'
  description 'Customizing Redmine: custom assets for all redmine'
  version '0.0.1'
  url 'https://bitbucket.org/bashbuilder/redmine_custom_assets'
end

